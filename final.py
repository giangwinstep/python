import numpy as np
from numpy import linalg as LA

m, n = map(int, input().split())
X = []
result_a = []
result_b = []


def check(arrayX, array1, array2):
    a = LA.norm(arrayX - array1)
    b = LA.norm(arrayX - array2)

    if a < b:
        return 1
    elif a == b:
        return 2
    else:
        return 3


for i in range(m):
    temp = list(map(float, input().split()))
    X.append(temp)

a = list(map(float, input().split()))
b = list(map(float, input().split()))

X = np.array(X)
a = np.array(a)
b = np.array(b)

for i in range(m):
    if check(X[i], a, b) == 1:
        z = X[i].tolist()
        result_a.append(z)
    elif check(X[i], a, b) == 3:
        y = X[i].tolist()
        result_b.append(y)

result_a = np.array(result_a)
result_b = np.array(result_b)
print(result_a)
print(result_b)